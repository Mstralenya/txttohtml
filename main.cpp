#include <QtCore/QCoreApplication>
#include <QTextStream>
#include <QFile>
#include <iostream>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    if (argc!=3)
    {
        std::cout<<"Invalid cout of arguments";
        return 0;
    }
    QFile input(argv[1]),output(argv[2]),html1(":/html/begin.txt"),html2(":/html/end.txt");
    html1.open(QIODevice::ReadOnly|QIODevice::Text);
    html2.open(QIODevice::ReadOnly|QIODevice::Text);
    input.open(QIODevice::ReadOnly|QIODevice::Text);
    output.open(QIODevice::WriteOnly|QIODevice::Text);
    QTextStream out(&output);
    out<<html1.readAll()<<input.readAll()<<html2.readAll();
    html1.close();
    html2.close();
    output.close();
    input.close();
    return 0;
}
