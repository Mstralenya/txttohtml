#Definition:
This is simple [tool](https://github.com/mstralenya/txttohtml) for wrapping text file into bootstrap-powered html page.
#Usage:
name_of_executable.exe input_file output_file
Where input_file ­ name of file which contains text that will be wrapped,
output_file ­ name of html file which will be generated.
In case of wrong count of arguments program automaticall will be closed with message.
#Bugs:
tell me!
#Version history:
##0.01a - added text files for resource generating
##0.01  ­ initial release
