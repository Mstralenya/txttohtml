#-------------------------------------------------
#
# Project created by QtCreator 2012-10-09T15:44:06
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = txt_to_htm
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

RESOURCES += \
    RESOURCES.qrc
